function div(a, b){
    return (a - a % b) / b;
}

function qsort2(A, st, end){
    if (end - st < 1)
        return;
    let supp = st, st2 = st + 1, end2 = end;
    while (st2 <= end2){
        while (A[end2] >= A[supp])
            --end2;
        if (end2 <= supp)
            break;
        let tmp = A[supp];
        A[supp] = A[end2];
        A[end2] = tmp;
        supp = end2;
        --end2;
        
        while (A[st2] <= A[supp])
            ++st2;
        if (st2 >= supp)
            break;
        tmp = A[supp];
        A[supp] = A[st2];
        A[st2] = tmp;
        supp = st2;
        ++st2;
    }
    qsort2(A, st, supp-1);
    qsort2(A, supp+1, end);
}

function qsort(A){
    qsort2(A, 0, A.length-1);
}