function FirstN_Fib(n){
    let sum = 1;
    if (n < 1) return -1;
    else if (n == 1) return sum;
    else{
        let x = 1, prev = 1;
        for (; n > 1; --n){
            sum += x;
            let tmp = x;
            x += prev;
            prev = tmp;
        }
        return sum;
    }
}